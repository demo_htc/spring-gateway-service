package com.htc.ea.springgatewayservice.fallback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServicesFallback {

	private Logger log = LoggerFactory.getLogger(getClass());
	private static final String ERROR = "Error";
	
	/**
	 * metodo de respuesta en caso de falla
	 * @return response entity con descripcion en el header y codigo de estado time out
	 */
	@GetMapping("/serviceFallback")
	public ResponseEntity<Object> serviceFallback(){
		log.error("Entrada al metodo de error de respaldo");
		HttpHeaders headers = new HttpHeaders();
		headers.add(ERROR, "No se pudo acceder al servicio solicitado");
		return new ResponseEntity<>(null,headers,HttpStatus.GATEWAY_TIMEOUT);
	}
}
